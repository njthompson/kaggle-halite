import base64

import numpy
import torch
from kaggle_environments.envs.halite import helpers

#############################################
# Only the following can be imported: Python Standard Library Modules,
#       gym, numpy, scipy, pytorch (1.3.1, cpu only)
#############################################

PLAYER_MAX_SHIPS = 25
PLAYER_MAX_SHIPYARDS = 6
ENV_MAP_SIZE = 21

MODEL_STRING = None


class PyTorchMlp(torch.nn.Module):

    def __init__(self, n_inputs=126, n_actions=7):
        torch.nn.Module.__init__(self)

        self.fc1 = torch.nn.Linear(n_inputs, 64)
        self.fc2 = torch.nn.Linear(64, 64)
        self.fc3 = torch.nn.Linear(64, n_actions)
        self.activ_fn = torch.nn.Tanh()
        self.out_activ = torch.nn.Softmax(dim=1)

    def forward(self, x):
        x = self.activ_fn(self.fc1(x))
        x = self.activ_fn(self.fc2(x))
        x = self.out_activ(self.fc3(x))
        return x


def create_observation_space(observations, configuration, map_size, ship_id):
    """Takes halite env and creates the observation space"""

    # Observation space has shape (4, map_size, map_size)
    # Ships map, between -1 and 1, having |1| represents the most halite on a ship in the game.
    #   Negative values are enemies, positive are friendly
    # Halite map, between 0 and 1, normalised to max halite on board
    # Shipyards map, -1 for enemy shipyards, +1 for friendly
    # Lead ship map, one-hot representation of the leading ships location

    ships_map = numpy.zeros((map_size, map_size))
    ship_yards_map = numpy.zeros((map_size, map_size))
    halite_map = numpy.zeros((map_size, map_size))

    board = helpers.Board(observations, configuration)

    # Ships array
    ships = [(i.halite, i.player_id, i.position) for _, i in board.ships.items()]
    max_ship_halite = max([i for i, _, _ in ships] + [0])
    if max_ship_halite == 0:
        max_ship_halite = 1

    player_multiplier = [1 if i == board.current_player.id else -1 for _, i, _ in ships]
    for i, (halite, _, position) in enumerate(ships):
        ships_map[position] = player_multiplier[i] * halite / max_ship_halite

    # Shipyards array
    ship_yards = [(i.player_id, i.position) for _, i in board.shipyards.items()]
    player_multiplier = [1 if i == board.current_player.id else -1 for i, _ in ship_yards]
    for i, (_, position) in enumerate(ship_yards):
        ship_yards_map[position] = player_multiplier[i]

    # Halite array
    for location, obj in board.cells.items():
        halite_map[obj.position] = obj.halite

    # Scale from 0 to 1
    halite_map /= numpy.max(halite_map)

    # Get one hot lead ship map
    location_map = numpy.zeros((map_size, map_size))
    if ship_id is not None:
        location_map[board.ships[ship_id].position] = 1

    else:
        # No current ships
        pass

    # Transpose
    ships_map = ships_map.transpose()
    ship_yards_map = ship_yards_map.transpose()
    halite_map = halite_map.transpose()
    location_map = location_map.transpose()

    # Format observations output
    return numpy.array([ships_map, ship_yards_map, halite_map, location_map]).astype(numpy.float32)


def agent(observation, configuration):
    """Runs the model to create actions"""

    with open("model.dat", "wb") as f:
        f.write(base64.b64decode(MODEL_STRING))

    model = PyTorchMlp(n_inputs=1764, n_actions=6)
    model.load_state_dict(torch.load("model.dat"))

    ship_action_map = {
        0: None,
        1: helpers.ShipAction.NORTH,
        2: helpers.ShipAction.EAST,
        3: helpers.ShipAction.SOUTH,
        4: helpers.ShipAction.WEST,
        5: helpers.ShipAction.CONVERT,
    }

    board = helpers.Board(observation, configuration)

    ship = None
    for ship in board.current_player.ships:
        obs = create_observation_space(
            observations=observation,
            configuration=configuration,
            map_size=ENV_MAP_SIZE,
            ship_id=ship.id
        )

        with torch.no_grad():
            model_actions = model(torch.from_numpy(obs).view(1, -1).float()).numpy()[0]
            print(model_actions)
            model_actions = numpy.argmax(model_actions)
            ship.next_action = ship_action_map[model_actions]

        # ship_action = random.choice(["NORTH", "SOUTH", "EAST", "WEST", None])
        # ship.next_action = ship_action_map[1]

    # If no shipyards exist, make one
    if not board.current_player.shipyards and ship is not None:
        ship.next_action = ship_action_map[5]

    # Create actions for shipyards
    ship_locations = [i.position for i in board.current_player.ships]
    total_halite, _, _ = observation.players[observation.player]
    for i, shipyard in enumerate(board.current_player.shipyards):

        # Check constraints for spawning a new ship
        if board.current_player.halite > 2000 \
                and shipyard.position not in ship_locations \
                and len(ship_locations) < 15:
            shipyard.next_action = helpers.ShipyardAction.SPAWN

    print(board)
    print(board.current_player.next_actions)
    return board.current_player.next_actions
