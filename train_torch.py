# Filter tensorflow version warnings

import random

import os

# https://stackoverflow.com/questions/40426502/is-there-a-way-to-suppress-the-messages-tensorflow-prints/40426709
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # or any {'0', '1', '2'}
import warnings

# https://stackoverflow.com/questions/15777951/how-to-suppress-pandas-future-warning
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=Warning)

import numpy as np
import sys
from kaggle_environments import evaluate, make
from kaggle_environments.envs.halite import helpers
from stable_baselines3 import PPO, A2C
from stable_baselines3.common import env_checker
from stable_baselines3.common.save_util import save_to_zip_file
from stable_baselines3.common.torch_layers import is_image_space
from stable_baselines3.common.callbacks import BaseCallback
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.results_plotter import load_results, ts2xy
from stable_baselines3.common.vec_env import DummyVecEnv

from halite_helper_torch import HaliteEnv, create_observation_space

# # https://stackoverflow.com/questions/15777951/how-to-suppress-pandas-future-warning
# warnings.simplefilter(action='ignore', category=FutureWarning)
# warnings.simplefilter(action='ignore', category=Warning)

np.set_printoptions(threshold=sys.maxsize)

#############################################
# Only the following can be imported: Python Standard Library Modules,
#       gym, numpy, scipy, pytorch (1.3.1, cpu only)
#############################################

VERSION = "1_0_3"
DIRECTORY_LOGS = "halite_logs/"
DIRECTORY_SAVED_MODELS = "models/"
DIRECTORY_REPLAYS = "replays/"

ENV_MAP_SIZE = 21
ENV_STARTING_HALITE = 1000

PLAYER_MAX_SHIPS = 25
PLAYER_MAX_SHIPYARDS = 6


class SaveOnBestTrainingRewardCallback(BaseCallback):
    """
    Callback for saving a model (the check is done every ``check_freq`` steps)
    based on the training reward (in practice, we recommend using ``EvalCallback``).

    :param check_freq: (int)
    :param log_dir: (str) Path to the folder where the model will be saved.
      It must contains the file created by the ``Monitor`` wrapper.
    :param verbose: (int)
    """

    def __init__(self, check_freq: int, save_dir: str, log_dir: str, verbose=1):

        super(SaveOnBestTrainingRewardCallback, self).__init__(verbose)
        self.check_freq = check_freq
        self.log_dir = log_dir
        self.save_path = save_dir
        self.best_mean_reward = -np.inf

    def _init_callback(self) -> None:

        # Create folder if needed
        if self.save_path is not None:
            os.makedirs(self.save_path, exist_ok=True)

    def _on_step(self) -> bool:
        if self.n_calls % self.check_freq == 0:

            # Retrieve training reward
            x, y = ts2xy(load_results(self.log_dir), 'timesteps')
            if len(x) > 0:

                # Mean training reward over the last 100 episodes
                mean_reward = np.mean(y[-100:])
                if self.verbose > 0:
                    print(f"Num timesteps: {self.num_timesteps}")
                    print(f"Best mean reward: {self.best_mean_reward:.2f} - Last mean reward "
                          f"per episode: {mean_reward:.2f}")

                # New best model, you could save the agent here
                if mean_reward > self.best_mean_reward:
                    self.best_mean_reward = mean_reward

                    # Example for saving best model
                    if self.verbose > 0:
                        print(f"Saving new best model to {self.save_path}.zip")
                    self.model.save(self.save_path)

        return True


def debug_env(env):
    """Runs the environment step by step"""
    obs = env.reset()
    n_steps = 100
    for step in range(n_steps):
        print(f"Step: {step}")

        action = env.action_space.sample()
        obs, reward, done, info = env.step(action)

        for halite, shipyards, ships in env.obs.players:
            print(f"Halite: {halite}, Shipyards: {len(shipyards)}, Ships: {len(ships)}, Reward: {reward}")

        env.render_map()

        if done:
            break


def instantiate_model(env, model_name, load=True):
    # Check env
    env_checker.check_env(env, warn=True)
    monitor_env = Monitor(env, DIRECTORY_LOGS, allow_early_resets=True)
    vec_env = DummyVecEnv([lambda: monitor_env])

    # Create/save a model
    if not load:
        model = PPO("MlpPolicy", vec_env, verbose=2)
        print(f"Created PPO model")

    # Load a model
    else:
        model = PPO.load(model_name, vec_env, verbose=2)
        model.get_torch_variables()
        print(f"Loaded PPO model")

    return model, model_name


def create_callable_model(model, env):
    """Returns a function that takes observations and configuration as input"""

    def agent(observations, configuration):
        """Runs the model to create actions"""

        ship_action_map = {
            0: None,
            1: helpers.ShipAction.NORTH,
            2: helpers.ShipAction.EAST,
            3: helpers.ShipAction.SOUTH,
            4: helpers.ShipAction.WEST,
            5: helpers.ShipAction.CONVERT,
        }

        board = helpers.Board(observations, configuration)

        for ship in board.current_player.ships:
            model_actions, _ = model.predict(
                observation=create_observation_space(
                    observations=observations,
                    configuration=configuration,
                    map_size=ENV_MAP_SIZE,
                    ship_id=ship.id
                )
            )

            ship.next_action = ship_action_map[model_actions]

        # Create actions for shipyards
        total_halite, _, _ = observations.players[observations.player]
        for i, shipyard in enumerate(board.current_player.shipyards):
            shipyard.next_action = np.random.choice(
                a=[helpers.ShipyardAction.SPAWN, None],
                p=[0.5, 0.5]  # This can be changed to weigh decisions
            )

        return board.current_player.next_actions

    return agent


def save_params(model):
    # save model parameters
    params = model.get_parameters()
    with open("model_parameters.txt", "w") as f:
        for k, v in params.items():
            f.write(f"'{k}': {str(v)}, \n")


def main(debug=False, load=True, train=False, save_torch=True):
    """Main code for running the script"""

    os.makedirs(DIRECTORY_SAVED_MODELS, exist_ok=True)
    os.makedirs(DIRECTORY_LOGS, exist_ok=True)
    os.makedirs(DIRECTORY_REPLAYS, exist_ok=True)

    # Create env
    env = HaliteEnv(
        opponents=["random", "random", "random"],
        # opponents=[swarm_agent, swarm_agent, swarm_agent],
        replay_save_dir=DIRECTORY_REPLAYS,
        configuration={
            "size": ENV_MAP_SIZE,
            "startingHalite": ENV_STARTING_HALITE,
            "max_ships": PLAYER_MAX_SHIPS,
            "max_shipyards": PLAYER_MAX_SHIPYARDS,
        }
    )

    # Run an example (debugging)
    if debug:
        debug_env(env)

    # Load a model
    model_name = f"{DIRECTORY_SAVED_MODELS}/pytorch_3"
    model, model_name = instantiate_model(env, model_name, load=load)

    # Train and test model
    if train:
        callback = SaveOnBestTrainingRewardCallback(
            check_freq=200,
            save_dir=model_name,
            log_dir=DIRECTORY_LOGS,
            verbose=2)
        model.learn(15000, callback=callback)

    # Save the raw torch model
    if save_torch:
        pass

    # Establish model inside an agent
    agent = create_callable_model(model, env)

    # Evaluate agent
    print("Beginning evaluation")

    outcomes = evaluate(
        environment="halite",
        agents=[agent, "random", "random", "random"],
        # agents=[agent, swarm_agent, "random", swarm_agent],
        num_episodes=10,
        configuration={
            "size": ENV_MAP_SIZE,
            "startingHalite": ENV_STARTING_HALITE,
            "agentExec": "LOCAL"
        }
    )

    print(outcomes)
    mean_reward(outcomes)

    print(mean_reward(outcomes))
    print("Debug me")


def random_agent(obs):
    action = {}
    ship_id = list(obs.players[obs.player][2].keys())[0]
    ship_action = random.choice(["NORTH", "SOUTH", "EAST", "WEST", None])
    if ship_action is not None:
        action[ship_id] = ship_action
    return action


def mean_reward(rewards):
    wins = 0
    ties = 0
    losses = 0
    for r in rewards:
        r0 = 0 if r[0] is None else r[0]
        r1 = 0 if r[1] is None else r[1]
        if r0 > r1:
            wins += 1
        elif r1 > r0:
            losses += 1
        else:
            ties += 1

    return f'wins: {wins} ({wins / len(rewards)}), ties: {ties} ({ties / len(rewards)}), ' \
           f'losses: {losses} ({losses / len(rewards)})'


def test_submission():
    env = make("halite", debug=True)
    env.run_preprocessing(["submission_base.py", "random"])
    print("Debug")


if __name__ == '__main__':
    main()
    # test_submission()
