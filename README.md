# kaggle-halite

Halite Kaggle competition. Aim was to create a submission that would compete in a simulated game against other player submissions. This repo looks to solve this using a simple RL approach using Stable Baselines.

In particular, the files `train_tf`, `train_torch`, and the `lib/helper` files are key in this repo.