# Filter tensorflow version warnings

import random

import os

# https://stackoverflow.com/questions/40426502/is-there-a-way-to-suppress-the-messages-tensorflow-prints/40426709
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # or any {'0', '1', '2'}
import warnings

# https://stackoverflow.com/questions/15777951/how-to-suppress-pandas-future-warning
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=Warning)

import numpy as np
import copy
import gym


from kaggle_environments import make
from kaggle_environments.envs.halite import helpers

MAP_SIZE = 21
CONVERT_COST = 500
SPAWN_COST = 500


def create_observation_space(observations, configuration, map_size, ship_id):
    """Takes halite env and creates the observation space"""

    # Observation space has shape (4, map_size, map_size)
    # Ships map, between -1 and 1, having |1| represents the most halite on a ship in the game.
    #   Negative values are enemies, positive are friendly
    # Halite map, between 0 and 1, normalised to max halite on board
    # Shipyards map, -1 for enemy shipyards, +1 for friendly
    # Lead ship map, one-hot representation of the leading ships location

    ships_map = np.zeros((map_size, map_size))
    ship_yards_map = np.zeros((map_size, map_size))
    halite_map = np.zeros((map_size, map_size))

    board = helpers.Board(observations, configuration)

    # Ships array
    ships = [(i.halite, i.player_id, i.position) for _, i in board.ships.items()]
    max_ship_halite = max([i for i, _, _ in ships] + [0])
    if max_ship_halite == 0:
        max_ship_halite = 1

    player_multiplier = [1 if i == board.current_player.id else -1 for _, i, _ in ships]
    for i, (halite, _, position) in enumerate(ships):
        ships_map[position] = player_multiplier[i] * halite / max_ship_halite

    # Shipyards array
    ship_yards = [(i.player_id, i.position) for _, i in board.shipyards.items()]
    player_multiplier = [1 if i == board.current_player.id else -1 for i, _ in ship_yards]
    for i, (_, position) in enumerate(ship_yards):
        ship_yards_map[position] = player_multiplier[i]

    # Halite array
    for location, obj in board.cells.items():
        halite_map[obj.position] = obj.halite

    # Scale from 0 to 1
    halite_map /= np.max(halite_map)

    # Get one hot lead ship map
    location_map = np.zeros((map_size, map_size))
    if ship_id is not None:
        location_map[board.ships[ship_id].position] = 1

    else:
        # No current ships
        pass

    # Transpose
    ships_map = ships_map.transpose()
    ship_yards_map = ship_yards_map.transpose()
    halite_map = halite_map.transpose()
    location_map = location_map.transpose()

    # Format observations output
    return np.array([ships_map, ship_yards_map, halite_map, location_map]).astype(np.float32)


class HaliteEnv(gym.Env):
    """Helper class for creating a halite env. This env only allows control for a single agent at a time"""

    def __init__(
            self,
            opponents,
            replay_save_dir='',
            **kwargs
    ):

        super(HaliteEnv, self).__init__()

        self.env = make('halite', **kwargs)
        self.trainer = self.env.train([None, *opponents])
        # self.trainer = self.env.train([None, *opponents])
        self.replay_save_dir = replay_save_dir
        self.obs = None

        # Ship ID this agent will play as
        self.lead_ship_id = None

        self.map_size = kwargs["configuration"]["size"]
        self.max_ships = int(kwargs["configuration"]["max_ships"])
        self.max_shipyards = int(kwargs["configuration"]["max_shipyards"])

        # Observation space
        #   - ships map
        #   - shipyard map
        #   - resources map
        #   - One hot location
        self.observation_space = gym.spaces.Box(low=-1, high=1, shape=(4, self.map_size, self.map_size))

        # Action space
        #   - Ships:     Discrete 6 - Hold[0], North[1], EAST[2], SOUTH[3], WEST[4], Convert[5]
        #   - Shipyards: Discrete 2 - Hold[0], Spawn Ship[1]
        self.action_space = gym.spaces.Discrete(6)

        self.spec = None
        self.metadata = None

    def reset(self):
        """Reset trainer environment"""

        self.obs = copy.deepcopy(self.trainer.reset())

        # self.lead_ship_id = "0-1"  # Assume always player zero ship one
        self.set_lead_ship()

        return create_observation_space(self.obs, self.env.configuration, self.map_size, self.lead_ship_id)

    def step(self, actions):
        """Step forward in actual environment"""

        # Convert to halite actions
        halite_actions = self.create_actions(actions)

        # Record last observations for calculating reward
        prior_obs = copy.deepcopy(self.obs)

        # Take simulation step
        self.obs, reward, done, info = self.trainer.step(halite_actions)

        # Set own reward system
        updated_reward = self.generate_reward(done, prior_obs, halite_actions)

        # Update observations, record info
        self.obs = copy.deepcopy(self.obs)
        info = {}

        # Set new lead ship
        self.set_lead_ship()

        # Create new observations
        obs = create_observation_space(self.obs, self.env.configuration, self.map_size, self.lead_ship_id)

        return obs, updated_reward, done, info

    def set_lead_ship(self):
        """When reset, and on every new turn, allocate a new ship for the model to control"""

        board = helpers.Board(self.obs, self.env.configuration)
        ships = board.current_player.ships
        if not ships:
            self.lead_ship_id = None
        else:
            self.lead_ship_id = ships[0].id

    def generate_reward(self, done, prior_observations, actions):
        """
        Identifies the reward that should be given to the lead ship for the previous turn. By
        nature of how we're defining this model, we need the rewards to be very much single-ship
        specific. We can't really deal rewards out for holistic player improvements, needs to be
        specific to a single ships actions.

        We want each of these ship actions to be scaled accordingly, i.e. converting to a
        shipyard should be just as beneficial as mining for Halite.
        """

        reward = 0

        old_board = helpers.Board(prior_observations, self.env.configuration)
        board = helpers.Board(self.obs, self.env.configuration)

        # Check game reward
        if done:

            # If game is finished, check who's on top
            game_halite = {k: v.halite for k, v in board.players.items()}
            player_halite = game_halite[board.current_player.id]

            # Player has won (or tied)
            if player_halite == max(game_halite.values()):
                reward += 10

            # Player has lost
            else:
                reward -= 10

        # Game isn't done, add small reward for surviving s.t. 3 points are rewarded to a full game
        else:
            reward += 3 / self.env.configuration["episodeSteps"]

        # For the lead ship, what could have happened
        #   - Ship moved, mined halite
        #   - Ship deposited halite
        #   - Ship converted to shipyard
        #   - Ship made contact

        if self.lead_ship_id not in old_board.ships:
            return reward  # no unique reward here, occurs when there are no ships
        else:
            old_lead = old_board.ships[self.lead_ship_id]

        # Action selected last turn
        lead_action = actions[self.lead_ship_id] if self.lead_ship_id in actions else None

        # Old and current halite
        past_halite = old_board.current_player.halite
        current_halite = board.current_player.halite

        # Ship is still alive, and in players ships list
        if self.lead_ship_id in board.ships:

            current_lead = board.ships[self.lead_ship_id]

            # Ship didn't move, standard reward plus if they mined extra
            if lead_action is None:
                gained = current_lead.halite - old_lead.halite
                reward += 0.5 + gained / self.env.configuration["maxCellHalite"]

            # Ship passed over shipyard, depositing halite
            if current_lead.position in [i.position for i in board.current_player.shipyards]:
                deposited = old_lead.halite

                # No reward for passing through
                if deposited > 0:
                    reward += 0.5 + deposited * 0.005

        # Ship is no longer in ships list
        else:

            # Ship has been converted, deposited what halite it had
            if lead_action == "CONVERT":

                # Encourage more shipyards if we're under, penalise too many
                factor = self.env.configuration["max_shipyards"] - len(board.current_player.shipyards)
                reward += 0.2 * factor + old_lead.halite * 0.005

            # Ship is no longer alive, and wasn't converted, assume it has been destroyed
            else:
                reward += - 0.5 - old_lead.halite * 0.005

        return reward

    def create_actions(self, action):
        """Takes raw model inputs and translates them to game movements"""
        # Action space
        #   - Ships:     Discrete 6 - Hold[0], North[1], EAST[2], SOUTH[3], WEST[4], Convert[5]
        #   - Shipyards: Discrete 1 - Hold[0], Spawn Ship[1]

        ship_action_map = {
            0: None,
            1: helpers.ShipAction.NORTH,
            2: helpers.ShipAction.EAST,
            3: helpers.ShipAction.SOUTH,
            4: helpers.ShipAction.WEST,
            5: helpers.ShipAction.CONVERT,
        }

        board = helpers.Board(self.obs, self.env.configuration)

        total_halite, _, _ = self.obs.players[self.obs.player]

        # First, take agent action (the first ship), if no lead ship exists, this doesn't apply
        lead_agent_action = ship_action_map[action]

        # Iterate through player ships, allocating moves
        # TODO eventually, use pre-trained models to predict other ships moves
        possible_moves = list(ship_action_map.values())
        for ship in board.current_player.ships:
            if ship.id == self.lead_ship_id:
                ship.next_action = lead_agent_action
            else:
                ship.next_action = random.choice(possible_moves)

        # Create actions for shipyards
        for i, shipyard in enumerate(board.current_player.shipyards):
            # Heuristic for spawning ships, always spawn if possible
            shipyard.next_action = np.random.choice(
                a=[helpers.ShipyardAction.SPAWN, None],
                p=[0.5, 0.5]  # This can be changed to weigh decisions
            )

        return board.current_player.next_actions

    def render_map(self):
        print(helpers.Board(self.obs, self.env.configuration))
