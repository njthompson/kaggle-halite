# Filter tensorflow version warnings
import os

# https://stackoverflow.com/questions/40426502/is-there-a-way-to-suppress-the-messages-tensorflow-prints/40426709
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # or any {'0', '1', '2'}
import warnings

# https://stackoverflow.com/questions/15777951/how-to-suppress-pandas-future-warning
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=Warning)

import numpy as np

import torch as th
import torch.nn as nn


def copy_mlp_weights(baselines_model, n_inputs, n_actions):
    torch_mlp = PyTorchMlp(n_inputs=n_inputs, n_actions=n_actions)
    model_params = baselines_model.get_parameters()

    policy_keys = [key for key in model_params.keys() if "pi" in key]
    policy_params = [model_params[key] for key in policy_keys]

    for (th_key, pytorch_param), key, policy_param in zip(torch_mlp.named_parameters(), policy_keys, policy_params):
        param = th.from_numpy(policy_param)
        # Copies parameters from baselines model to pytorch model
        print(th_key, key)
        print(pytorch_param.shape, param.shape, policy_param.shape)
        pytorch_param.data.copy_(param.data.clone().t())

    return torch_mlp


class PyTorchMlp(nn.Module):

    def __init__(self, n_inputs=126, n_actions=7):
        nn.Module.__init__(self)

        self.fc1 = nn.Linear(n_inputs, 64)
        self.fc2 = nn.Linear(64, 64)
        self.fc3 = nn.Linear(64, n_actions)
        self.activ_fn = nn.Tanh()
        self.out_activ = nn.Softmax(dim=1)

    def forward(self, x):
        x = self.activ_fn(self.fc1(x))
        x = self.activ_fn(self.fc2(x))
        x = self.out_activ(self.fc3(x))
        return x
