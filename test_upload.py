from kaggle_environments import make


def main():

    env = make("halite", debug=True)

    # Play against yourself without an ERROR or INVALID.
    env.run_preprocessing(["agent.py", "agent.py"])
    print("EXCELLENT SUBMISSION!" if env.toJSON()["statuses"] == ["DONE", "DONE"] else "MAYBE BAD SUBMISSION?")


if __name__ == '__main__':
    main()