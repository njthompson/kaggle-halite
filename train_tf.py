# Filter tensorflow version warnings
import os

# https://stackoverflow.com/questions/40426502/is-there-a-way-to-suppress-the-messages-tensorflow-prints/40426709
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # or any {'0', '1', '2'}
import warnings

# https://stackoverflow.com/questions/15777951/how-to-suppress-pandas-future-warning
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=Warning)

import random
import base64
import numpy as np
from stable_baselines import PPO2, DQN
from stable_baselines.bench import Monitor
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines.common import env_checker

from kaggle_environments import evaluate, make
from kaggle_environments.envs.halite import helpers

from halite_helper_tf import HaliteEnv, create_observation_space
from swarm_intelligence import swarm_agent
import sys
import torch as th
from model_helper import copy_mlp_weights

from agent import agent

np.set_printoptions(threshold=sys.maxsize)

DIRECTORY_LOGS = "halite_logs/"
DIRECTORY_SAVED_MODELS = "models/"
DIRECTORY_REPLAYS = "replays/"

ENV_MAP_SIZE = 21
ENV_STARTING_HALITE = 5000

PLAYER_MAX_SHIPS = 25
PLAYER_MAX_SHIPYARDS = 6


def debug_env(env):
    """Runs the environment step by step"""
    obs = env.reset()
    n_steps = 100
    for step in range(n_steps):
        print(f"Step: {step}")

        action = env.action_space.sample()
        obs, reward, done, info = env.step(action)

        for halite, shipyards, ships in env.obs.players:
            print(f"Halite: {halite}, Shipyards: {len(shipyards)}, Ships: {len(ships)}, Reward: {reward}")

        env.render_map()

        if done:
            break


def run_training(model, model_name, loops=10, steps=5000):
    """Train and save the model, do this in a loop to enforce regular saving"""
    training_loops = loops
    for i in range(training_loops):
        model.learn(total_timesteps=steps)
        model.save(model_name)
        print(f"Model saved, training loop {i + 1} finished")


def instantiate_model(env, model_name, load=True):
    # Check env
    env_checker.check_env(env, warn=True)
    monitor_env = Monitor(env, DIRECTORY_LOGS, allow_early_resets=True)
    vec_env = DummyVecEnv([lambda: monitor_env])

    # Create/save a model
    if not load:
        model = PPO2("MlpPolicy", vec_env, verbose=2)

    # Load a model
    else:
        model = PPO2.load(model_name, vec_env, verbose=2)

    return model, model_name


def create_callable_model(model, env):
    """Returns a function that takes observations and configuration as input"""

    def agent(observations, configuration):
        """Runs the model to create actions"""

        ship_action_map = {
            0: None,
            1: helpers.ShipAction.NORTH,
            2: helpers.ShipAction.EAST,
            3: helpers.ShipAction.SOUTH,
            4: helpers.ShipAction.WEST,
            5: helpers.ShipAction.CONVERT,
        }

        board = helpers.Board(observations, configuration)
        model.eval()

        ship = None
        for ship in board.current_player.ships:
            obs = create_observation_space(
                observations=observations,
                configuration=configuration,
                map_size=ENV_MAP_SIZE,
                ship_id=ship.id
            )

            with th.no_grad():
                model_actions = model(th.from_numpy(obs).view(1, -1).float()).numpy()[0]
                model_actions = np.argmax(model_actions[0])
                ship.next_action = ship_action_map[model_actions]

        # If no shipyards exist, make one
        if not board.current_player.shipyards and ship is not None:
            ship.next_action = ship_action_map[5]

        # Create actions for shipyards
        ship_locations = [i.position for i in board.current_player.ships]
        total_halite, _, _ = observations.players[observations.player]
        for i, shipyard in enumerate(board.current_player.shipyards):

            # Check constraints for spawning a new ship
            if board.current_player.halite > 2000 \
                    and shipyard.position not in ship_locations \
                    and len(ship_locations) < 15:
                shipyard.next_action = helpers.ShipyardAction.SPAWN

        print(board)
        print(board.current_player.next_actions)
        return board.current_player.next_actions

    return agent


def main(debug=True, load=True, train=False, save_model=True):
    """Main code for running the script"""

    os.makedirs(DIRECTORY_SAVED_MODELS, exist_ok=True)
    os.makedirs(DIRECTORY_LOGS, exist_ok=True)
    os.makedirs(DIRECTORY_REPLAYS, exist_ok=True)

    # Create env
    env = HaliteEnv(
        # opponents=[],
        opponents=["random", "random", "random"],
        replay_save_dir=DIRECTORY_REPLAYS,
        configuration={
            "size": ENV_MAP_SIZE,
            "startingHalite": ENV_STARTING_HALITE,
            "max_ships": PLAYER_MAX_SHIPS,
            "max_shipyards": PLAYER_MAX_SHIPYARDS,
        }
    )

    # Run an example (debugging)
    if debug:
        debug_env(env)

    # Load a model
    model_name = f"{DIRECTORY_SAVED_MODELS}/torch_model_agent_v2"
    model, model_name = instantiate_model(env, model_name, load=load)

    # Train and test model
    if train:
        run_training(model, model_name, 5, 5000)

    # Send to PyTorch
    th_model = copy_mlp_weights(baselines_model=model, n_inputs=4 * ENV_MAP_SIZE * ENV_MAP_SIZE, n_actions=6)

    # Sanity check
    th_model.eval()
    sample = env.observation_space.sample()
    pred = th_model(th.from_numpy(sample).view(1, -1).float())
    print(pred)

    # Save torch model
    if save_model:
        torch_model_name = "torch_mlp_v2.bin"
        th.save(th_model.state_dict(), torch_model_name)

        with open(torch_model_name, 'rb') as f:
            encoded_string = base64.b64encode(f.read())

        with open('lib/serialized_model.py', 'w') as f:
            f.write(f'model_string={encoded_string}')

    # Establish model inside an agent
    agent_local = create_callable_model(th_model, env)

    # Evaluate agent
    print("Beginning evaluation")

    outcomes = evaluate(
        environment="halite",
        agents=[agent_local, "random", "random", "random", ],
        # agents=[agent, "random", "random", "random", ],
        num_episodes=5,
        configuration={
            "size": ENV_MAP_SIZE,
            "startingHalite": ENV_STARTING_HALITE,
            "agentExec": "LOCAL"
        }
    )

    print(outcomes)
    mean_reward(outcomes)

    print(mean_reward(outcomes))
    print("Debug me")


def random_agent(obs):
    action = {}
    ship_id = list(obs.players[obs.player][2].keys())[0]
    ship_action = random.choice(["NORTH", "SOUTH", "EAST", "WEST", None])
    if ship_action is not None:
        action[ship_id] = ship_action
    return action


def mean_reward(rewards):
    wins = 0
    ties = 0
    losses = 0
    for r in rewards:
        r0 = 0 if r[0] is None else r[0]
        r1 = 0 if r[1] is None else r[1]
        if r0 > r1:
            wins += 1
        elif r1 > r0:
            losses += 1
        else:
            ties += 1

    return f'wins: {wins} ({wins / len(rewards)}), ties: {ties} ({ties / len(rewards)}), ' \
           f'losses: {losses} ({losses / len(rewards)})'


def test_submission():
    env = make("halite", debug=True)
    env.run_preprocessing(["submission_base.py", "random"])
    print("Debug")


if __name__ == '__main__':
    main()
    # to_pytorch_model()
    # test_submission()
